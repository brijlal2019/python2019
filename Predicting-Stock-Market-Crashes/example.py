#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 14 13:48:24 2019

@author: brij
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import gridspec
import os
import seaborn as sns
from pylab import rcParams
from collections import defaultdict
from scipy.optimize import curve_fit
import sys
import warnings
if not sys.warnoptions:
    warnings.simplefilter("ignore")

datasets_original_test = ['AAPL.csv']
dataset_names_test = ['aapl']
datasets_test = []
for d in datasets_original_test:
    data_original = pd.read_csv(d, index_col = 'Date')
    data_original.index = pd.to_datetime(data_original.index, format="%Y/%m/%d")
    data_ch = data_original['Close'].pct_change()
    datasets_test.append(data_ch)
df_returns = pd.concat(datasets_test, axis=1, join_axes=[datasets_test[0].index])
df_returns.columns = dataset_names_test
corr = df_returns.corr()
print("Correlations of daily returns between datasets:")
rcParams['figure.figsize'] = 10, 6
ax =sns.heatmap(corr, annot=True, cmap='rocket_r')

datasets_original_test = ['AAPL.csv']
dataset_names = ['aapl']
datasets = []
for d in datasets_original_test:
    data_original = pd.read_csv(d, index_col = 'Date')
    data_original.index = pd.to_datetime(data_original.index, format="%Y/%m/%d")
    data_norm = data_original['Close']/data_original['Close'][-1]
    data_ch = data_original['Close'].pct_change()
    window = 10
    data_vol = data_original['Close'].pct_change().rolling(window).std()
    data = pd.concat([data_original['Close'], data_norm, data_ch, data_vol],axis=1).dropna()
    data.columns = ['price','norm','ch','vol']
    datasets.append(data)
    
df_ch = [d['ch'] for d in datasets]
df_returns = pd.concat(df_ch, axis=1, join_axes=[datasets[0].index])
df_returns.columns = dataset_names
corr = df_returns.corr()
print('Correlations of daily returns between datasets (non-correlated datasets):')
ax = sns.heatmap(corr, annot=True, cmap='rocket_r')

rcParams['figure.figsize'] = 10, 3
plt_titles = ['AAPL since 1995']

for ds, t in zip(datasets, plt_titles):
    plt.plot(ds['price'], color='blue', linewidth=0.8)
    plt.grid()
    plt.legend(['Price'])
    plt.title(t + ' - Price')
    plt.show()


    

